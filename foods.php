<?php
/**
 * 钉钉提醒 加班报餐提醒
 *
 * @author yougui@zuoyebang.com
 * @version 1.0
 * @brief 每周五 上午10：10发一次
 */
function request_by_curl($remote_server, $post_string) {  
    $ch = curl_init();  
    curl_setopt($ch, CURLOPT_URL, $remote_server);
    curl_setopt($ch, CURLOPT_POST, 1); 
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5); 
    curl_setopt($ch, CURLOPT_HTTPHEADER, array ('Content-Type: application/json;charset=utf-8'));
    curl_setopt($ch, CURLOPT_POSTFIELDS, $post_string);  
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);  
    // 线下环境不用开启curl证书验证, 未调通情况可尝试添加该代码
    // curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0); 
    // curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, 0);
    $data = curl_exec($ch);
    curl_close($ch);  
               
    return $data;  
}  

$access_token = "60614c238c2ec0db43fb5c18156f8af042b3f526541dc9ece23caf2eac7d6e0c";	//教学研发大群
//$access_token = "5532b263d53be0f7d7484e15c966f5cbb460579823034a6bdf6adcd77650370c"; //机器人Test

//api
$webhook = "https://oapi.dingtalk.com/robot/send?access_token=" . $access_token;


$title = "Attention!!";
//发送的内容 周报提醒

$data = [
    "msgtype" => "markdown",
    "markdown" => [
        'title' => "@所有人 $title",
        'text' => "### $title \n
1.周报（周五**18**点前完成），项目负责人更新：\n
[http://wiki.afpai.com/pages/viewpage.action?pageId=21440897](http://wiki.afpai.com/pages/viewpage.action?pageId=21440897) \n 
2.休假加班餐申请公告：\n
填写截止时间： 每周五（单休周六） **12**点前！！！！\n
法定节假日另行通知 \n
申请地址：    \n
[http://wiki.afpai.com/pages/viewpage.action?pageId=19907524](http://wiki.afpai.com/pages/viewpage.action?pageId=19907524) \n
@所有人
",
    ],
    "at" => [
        "isAtAll" => true,
    ],
];



$data_string = json_encode($data);

$result = request_by_curl($webhook, $data_string);  

$resultArr = json_decode($result, true);

echo sprintf("%s  %s  %s" . PHP_EOL, $result, date('Y-m-d H:i:s'), $title);

