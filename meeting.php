<?php
/**
 * 钉钉提醒 站会提醒
 *
 * @author yougui@zuoyebang.com
 * @version 1.0
 * @brief 每周一、三、五发
 */
function request_by_curl($remote_server, $post_string) {  
    $ch = curl_init();  
    curl_setopt($ch, CURLOPT_URL, $remote_server);
    curl_setopt($ch, CURLOPT_POST, 1); 
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5); 
    curl_setopt($ch, CURLOPT_HTTPHEADER, array ('Content-Type: application/json;charset=utf-8'));
    curl_setopt($ch, CURLOPT_POSTFIELDS, $post_string);  
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);  
    // 线下环境不用开启curl证书验证, 未调通情况可尝试添加该代码
    // curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0); 
    // curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, 0);
    $data = curl_exec($ch);
    curl_close($ch);  
               
    return $data;  
}  

$access_token = "dddbde29c91eee58b71d9ccf501e248a7e3f74fdc708e2d283f2dfcee79d2fb9";	//教学研发后端

//api
$webhook = "https://oapi.dingtalk.com/robot/send?access_token=" . $access_token;


$title = "站会提醒";
//发送的内容 站会提醒
$data = [
    "msgtype" => "text",
    "text" => [
        "content" => "10:20站会，请注意不要迟到！"
    ],
    "at" => [
        "isAtAll" => true,
    ],
];


$data_string = json_encode($data);

$result = request_by_curl($webhook, $data_string);  

$resultArr = json_decode($result, true);

echo sprintf("%s  %s  %s" . PHP_EOL, $result, date('Y-m-d H:i:s'), $title);
	

