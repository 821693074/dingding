# dingding

钉钉机器人

通过自定义机器人，定时通知群消息
> ###每周五 15点周报提醒
    0 15 * * 5 /usr/bin/php /home/yougui/dingding/dingding.php >> /home/yougui/logs/dingding.log 2>&1 &

> ###每周一、三、五 8:30站会提醒
    30 8 * * 1,3,5 /usr/bin/php /home/yougui/dingding/meeting.php >> /home/yougui/logs/dingding.log 2>&1 &

> ###每周五 每天上午10点数据报告
	nohup /usr/local/python3/bin/python3.6 /home/homework/sites/dingTalk.py > nohup.out 2>&1 & 