
#_*_coding:utf-8_*_

import schedule
import time
import datetime
import threading
import requests
import json
import codecs
import logging

from bs4 import BeautifulSoup
from urllib import request,parse


logger = logging.getLogger(__name__)
logger.setLevel(level = logging.INFO)
handler = logging.FileHandler("log.txt")
handler.setLevel(logging.INFO)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
handler.setFormatter(formatter)

logger.addHandler(handler)

def post_data(data):

    url = 'https://oapi.dingtalk.com/robot/send?access_token=60614c238c2ec0db43fb5c18156f8af042b3f526541dc9ece23caf2eac7d6e0c' #教学研发部大群
    #url = 'https://oapi.dingtalk.com/robot/send?access_token=02d847c29d95123bd7f6e1558aafce65dfb6428df2d7870941c0ff02cd80a635' #模考后端
    
    payload ={"msgtype":"text","text":{"content":data},'at':{'atMobiles':[],'isAtAll':1}}


    h = {
        'Content-Type':'application/json',
        'charset':'UTF-8',
    }

    logger.info(url)
    logger.info(payload)

    r = requests.post(url
                      , data=json.dumps(payload)
                      , headers=h
                      )

    logger.info(r)
    logger.info(r.text)

def job_task():
    threading.Thread(target=post_data).start()

def prints():

    localtime = time.asctime( time.localtime(time.time()) )
    logger.info("本地时间为 :" + localtime)
    
def readMsgFile():
    file_object = open('dingMsg.txt')
    try:
        file_context = file_object.read()
        data = json.loads(file_context)
        for msg in data['msg']:
            if msg['isSend']==0:
                post_data(msg['content'])
            

        
        
    finally:
        file_object.close()


def get_cookie():
    
    url = 'https://www.zybang.com/session/submit/login'

    headers = {
        'User-Agent':'Mozilla/4.0 (compatible; MSIE 5.5; Windows NT)',
    }
    data = {
        'phone':'15101136021',
        'password':'4a6629303c679cfa6a5a81433743e52c',
        'user_type':1,  
    }
    data = parse.urlencode(data).encode('utf-8')

    
    req = request.Request(url=url,headers=headers,data=data,method='POST')
    response = request.urlopen(req)


    rec = response.read().decode('utf-8')

    logger.info('登录成功 ' +  rec)


    return json.loads(rec)['data']['zybuss']
     

def download_page(url, cookie):

    cookies={'zybuss': cookie}
    return requests.get(url, cookies=cookies).content
 
 
def parse_html(html):
    soup = BeautifulSoup(html,"lxml")

    courseInfo = soup.find('div', attrs = {'class': 'panel-body'}).getText()

    movie_list_soup = soup.find('tbody')
    movie_name_list = []
 
    for movie_li in movie_list_soup.find_all('tr'):
        
        num = movie_li.find_all('th')[1].getText()
        if int(num) > 10000:
                lessonid = '章节ID： ' + movie_li.find_all('td')[0].getText()
                time = ' 上课时间： ' +movie_li.find_all('td')[2].getText()
                nums = ' 报名人数： ' + movie_li.find_all('th')[1].getText()


                movie_name_list.append(lessonid + nums + time)
    return movie_name_list, courseInfo, None

def readLecture():
    logger.info("readLecture start")
    cookie = get_cookie()

    date = time.strftime('%Y%m%d', time.localtime(time.time()))
    logger.info(date)
    
    url = 'http://platmis.zuoyebang.cc/livemonitor/lecture/index?date=' + date
    
    with codecs.open('lectureIndex.txt', 'wb', encoding='utf-8') as fp:
        while url:
            html = download_page(url, cookie)
            lectures, course, url = parse_html(html)
            fp.write(u'{lectures}\n'.format(lectures='\r\n'.join(lectures)))

            sendDingTalk(lectures, course)
            
        logger.info("本次任务结束")

def getNameOnDuty():
    windowsNames = {
    '20190131':'主讲：张艳超; PC:汪楚; 移动端:晓红; 后端:任开毓; 多媒体:申彭良; FE：张爱玲',
    '20190201':'主讲：沈延涛; PC:汪楚; 移动端:晓红; 后端:任开毓; 多媒体:申彭良; FE：张爱玲',
    '20190202':'主讲：郭芙伶; PC:汪楚; 移动端:晓红; 后端:柴金洋; 多媒体:申彭良; FE：张爱玲',
    '20190203':'主讲：张艳超; PC:永毅; 移动端:晓红; 后端:柴金洋; 多媒体:申彭良; FE：张爱玲',
    '20190204':'主讲：李冰; PC:尚林; 移动端:晓红,李赛; 后端:柴金洋; 多媒体:王黎明; FE:姚焕春',
    '20190205':'主讲：杨杰; PC:永毅; 移动端:晓红,李赛; 后端:柴金洋; 多媒体:于吉太; FE:姚焕春',
    '20190206':'主讲：熊英越; PC:汪楚; 移动端:晓红,李赛; 后端:李滕建; 多媒体:范文耀; FE:姚焕春',
    '20190207':'主讲：沈延涛; PC:尚林; 移动端:张飞,李赛; 后端:李滕建; 多媒体:王黎明; FE:李倩倩',
    '20190208':'主讲：郭芙伶; PC:永毅; 移动端:张飞,李赛; 后端:于永超; 多媒体:景金锋; FE:李倩倩',
    '20190209':'主讲：张艳超; PC:汪楚; 移动端:志芳,李赛; 后端:于永超; 多媒体:朱勇; FE:李倩倩',
    '20190210':'主讲：英越; PC:尚林; 移动端:王辉,李赛; 后端:赵伟; 多媒体:常明; FE:马国福',
    '20190211':'主讲：杨杰; PC:汪楚; 移动端:王辉,李赛; 后端:伟杰; 多媒体:李公宽; FE:马国福',
    '20190212':'主讲：沈延涛; PC:永毅; 移动端:王辉; 后端:伟杰; 多媒体:李公宽; FE:马国福',
    '20190213':'主讲：熊英越; PC:尚林; 移动端:王辉; 后端:伟杰; 多媒体:李公宽; FE:马国福',
    }
    d = time.strftime('%Y%m%d', time.localtime(time.time()))

    return str(windowsNames[d])

def sendDingTalk(lectures, course):

    data = 'null\n'
    if lectures:
        data = u'{lectures}\n'.format(lectures='\r\n'.join(lectures))
        
    a = '[广播] 大家早上好\n'
    name = '今日值班同学： ' + getNameOnDuty() + '\n' 
    nums = '今日课程总量： ' + course + '\n'
    lecList = '今日万人课list： '+ '\n' + data
    end = '[再见]'
    post_data(a + name + nums + lecList + end)
        

def main():

    logger.info("python start")

    #readLecture()
    #schedule.every(30).seconds.do(prints)
    schedule.every(60).minutes.do(prints)
    #schedule.every().thursday.at("08:00").do(post_data)
    schedule.every().day.at("10:00").do(readLecture)

    while True:
        schedule.run_pending()
        time.sleep(1)

if __name__ == '__main__':
    main()
